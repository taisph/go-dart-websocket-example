package main

import (
	"golang.org/x/net/websocket"
	"net/http"
	"log"
)

func EchoServer(ws *websocket.Conn) {
	buf := make([]byte, 512)
	c, err:= ws.Read(buf)
	if err != nil {
		log.Fatalf("error reading from websocket: %v", err)
	}
	log.Printf("Received %d bytes: %s", c, buf)

	for l, r := 0, c-1; l < r; l, r = l +1, r -1 {
		buf[l], buf[r] = buf[r], buf[l]
	}

	c, err = ws.Write(buf[:c])
	if err != nil {
		log.Fatalf("error writing from websocket: %v", err)
	}

	log.Printf("Sent %d bytes: %s", c, buf)
}

func main() {
	http.Handle("/echo", websocket.Handler(EchoServer))
	err := http.ListenAndServe(":12345", nil)
	if err != nil {
		panic("ListenAndServe: " + err.Error())
	}
}
